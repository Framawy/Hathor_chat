package egy.chat.hathor.hathor.Drawer;



import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.FrameLayout;
import android.support.v7.widget.Toolbar;
import android.widget.TableLayout;

import egy.chat.hathor.hathor.R;

import static egy.chat.hathor.hathor.R.layout.activity_base;

public class BaseActivity extends AppCompatActivity
{

    DrawerLayout fullView;
    Toolbar toolbar;
    FrameLayout frameLayout;
    public NavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void setContentView(int layoutResID) {
        DrawerLayout fullView = (DrawerLayout) getLayoutInflater().inflate(activity_base, null);
        FrameLayout activityContainer = (FrameLayout) fullView.findViewById(R.id.activity_content);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(fullView);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        View header = navigationView.getHeaderView(0);


        //Buttons
        //LinearLayout tracks = (LinearLayout) header.findViewById(R.id.track_layout);
        TableLayout new_chat_btn  = (TableLayout) header.findViewById(R.id.new_chat_btn);
        TableLayout new_group_btn = (TableLayout) header.findViewById(R.id.new_group_btn);
        TableLayout profile_btn   = (TableLayout) header.findViewById(R.id.profile_btn);
        TableLayout contacts_btn  = (TableLayout) header.findViewById(R.id.contacts_btn);
        TableLayout settings_btn  = (TableLayout) header.findViewById(R.id.settings_btn);
        TableLayout about_btn  = (TableLayout) header.findViewById(R.id.about_btn);


        //New Chat Button
        new_chat_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(getApplicationContext(), TracksActivity.class);
                //startActivity(intent);
            }
        });

        //New group chat button
        new_group_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(getApplicationContext(), ArtistActivity.class);
               // startActivity(intent);
            }
        });

        //Profile button
        profile_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(getApplicationContext(), AlbumActivity.class);
                //startActivity(intent);
            }
        });



        //Contacts button
        contacts_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
               // startActivity(intent);
            }
        });

        //Setting

        //about


    }





    @Override
    public void onBackPressed() {
        DrawerLayout lay = (DrawerLayout) findViewById(R.id.activity_container);
        if (lay.isDrawerVisible(GravityCompat.START)) {
            lay.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}



